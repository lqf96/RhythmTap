#! /usr/bin/env python
import json

def floor(a, b):
    return a//b*b if a%b<b-a%b else (a//b+1)*b

with open("Test.json") as f:
    d = json.load(f)
# Floor by 25
for r in d["beatmap"]:
    r[1] = floor(r[1], 25)
    if len(r)>=4:
        r[3] = floor(r[3], 125)
# Sort by begin time
d["beatmap"].sort(lambda a, b: cmp(a[1], b[1]))

# Save to file
with open("Test2.json", "w") as f:
    json.dump(d, f)
