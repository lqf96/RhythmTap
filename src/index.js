//[ Imports ]
//Libraries
import $ from "jquery";
import Vue from "vue";
//RhythmTap
import {
    import_beatmap,
    get_all_beatmaps,
    get_file,
    read_as_data_url
} from "./store";
import {GamePlay} from "./gameplay";

//[ Constants ]
//Colors
const colors = ["green", "blue", "purple", "red"];
//Current color
let current_color_index = 0;

//[ Functions ]
//Date time filter
Vue.filter("rt-date-time", (time) => {
    //Seconds
    let sec = time.getSeconds();
    sec = (sec<10)?("0"+sec):String(sec);
    //Minutes
    let min = time.getMinutes();
    min = (min<10)?("0"+min):String(min);
    //Hours
    let hour = time.getHours();
    hour = (hour<10)?("0"+hour):String(hour);

    return hour+":"+min+":"+sec;
});

//Time filter
Vue.filter("rt-time", (time) => {
    //Seconds
    let sec = time%60;
    sec = (sec<10)?("0"+sec):String(sec);
    //Minutes
    let min = Math.floor(time/60)%60;
    min = (min<10)?("0"+min):String(min);
    //Hours
    let hour = Math.floor(time/3600);
    hour = (hour<10)?("0"+hour):String(hour);

    return hour+":"+min+":"+sec;
});

//Run when scripts loaded
$(() => {
    //[ Constants & Variables ]
    //Scene class
    let scene_class = "rt-loading";
    //Begin time & current time
    const begin_time = Date.now();
    //Current beatmap
    let current_beatmap = null;
    //Player audio element
    let player_audio = new Audio();

    //[ Functions ]
    //File tree from entry
    function file_tree_from_entry(item)
    {   //File
        if (item.isFile)
            return new Promise((resolve) => {
                item.file((file) => {
                    resolve(file);
                });
            });
        //Folder
        else if (item.isDirectory)
            return new Promise((resolve) => {
                item.createReader().readEntries((entries) => {
                    entries = Array.from(entries);
                    Promise.all(entries.map(file_tree_from_entry)).then((files) => {
                        let folder = {};
                        for (let i=0;i<entries.length;i++)
                            folder[entries[i].name] = files[i];
                        resolve(folder);
                    });
                });
            });
    }

    //Vue element
    let app = new Vue({
        el: "body",
        //Data
        data: {
            //Game status
            status: {
                //Current time
                current_time: new Date(),
                uptime: 0,
                total_beatmaps: 0,
                played_beatmaps: 0
            },
            //Beatmaps
            beatmaps: [],
            //Current beatmap
            current_beatmap: null,
            //Current beatmap index
            current_beatmap_index: null,
            //Current difficulty
            current_diff: null,
            //Player
            player: {
                playing: false,
                show_volume: false
            },
            //Game prompts
            gameplay: {
                prompt: "",
                combo: 0,
                score: 0,
                accuracy: 1,
                point_gained: 0,
                stat_300: 0,
                stat_100: 0,
                stat_50: 0,
                stat_0: 0
            }
        },
        //Methods
        methods: {
            //Scene class
            set_scene: (_scene) => {
                $(".rt-scene").removeClass(scene_class)
                    .addClass(_scene);
                scene_class = _scene;
            },
            //Show import beatmap
            __show_import_beatmap: () => {
                $("#rt-import-beatmap").show();
            },
            //Hide beatmap
            __hide_import_beatmap: () => {
                $("#rt-import-beatmap").hide();
            },
            //Import beatmap (Drop event)
            __import_beatmap: function(event)
            {   //Traverse through selected files & folders
                let items = Array.from(event.dataTransfer.items);
                for (let item of items)
                {   item = item.webkitGetAsEntry();
                    if (item)
                        //Read file tree
                        file_tree_from_entry(item)
                            //Import beatmap
                            .then(import_beatmap)
                            //Add beatmap to beatmap list
                            .then((beatmap) => {
                                beatmap.__color = colors[current_color_index%colors.length];
                                current_color_index++;
                                //Add color to beatmap
                                this.beatmaps.push(beatmap);
                                //Set current beatmap to this beatmap
                                this.set_current_beatmap(this.beatmaps.length-1);
                            });
                }
                //Hide import beatmap prompt
                $("#rt-import-beatmap").hide();
            },
            //Set current beatmap
            set_current_beatmap: function(index)
            {   this.current_beatmap_index = index;
                let beatmap = this.current_beatmap = this.beatmaps[index];
                //Set difficulty
                this.set_difficulty(beatmap.difficulties[0]);

                //Set background image
                get_file(`/Beatmaps/${beatmap.id}/Assets/${beatmap.bgimage}`)
                    .then(read_as_data_url)
                    .then((url) => {
                        $(document.body).css("background-image", `url(${url})`);
                    });
                //Set player audio
                get_file(`/Beatmaps/${beatmap.id}/Assets/${beatmap.track}`)
                    .then(read_as_data_url)
                    .then((url) => {
                        //Play music
                        player_audio.pause();
                        player_audio.src = url;
                        player_audio.play();
                        //Set playing state and duration
                        this.player.playing = true;
                        this.player.duration = player_audio.duration;
                    });
            },
            //Play / pause audio
            play_pause_audio: function()
            {   const playing = this.player.playing = !this.player.playing;
                //Play
                if (playing)
                    player_audio.play();
                //Pause
                else
                    player_audio.pause();
            },
            //Show / hide volume
            show_hide_volume: function()
            {   this.player.show_volume = !this.player.show_volume;
            },
            //Set volume
            set_volume: function(event)
            {   const volume_ratio = event.offsetX/event.target.offsetWidth;
                $("#rt-player-volume").val(volume_ratio);
                player_audio.volume = volume_ratio;
            },
            //Begin game play
            begin_gameplay: function()
            {   if (!this.current_diff)
                    return;
                //Change scene and stop music
                this.set_scene("rt-game");
                if (this.player.playing)
                    this.play_pause_audio();
                //Start gameplay
                new GamePlay(this);
            },
            //Set current difficulty
            set_difficulty: function(diff)
            {   this.current_diff = diff;
            }
        }
    });

    //[ Game Initialization ]
    //Game scene
    app.set_scene("rt-welcome");

    //Game time
    setInterval(() => {
        //Current time
        const current_time = app.status.current_time = new Date();
        //Uptime
        app.status.uptime = Math.floor((current_time-begin_time)/1000);
    }, 1000);

    //Retrive beatmap metadatas
    get_all_beatmaps().then((beatmaps) => {
        //Set beatmap colors
        for (let beatmap of beatmaps)
        {   beatmap.__color = colors[current_color_index%colors.length];
            current_color_index++;
        }
        //Set beatmaps
        app.beatmaps = beatmaps;

        if (beatmaps.length>=1)
            app.set_current_beatmap(0);
    });

    //Set player audio
    player_audio.loop = true;
    //Song progress
    let player_progress = $("#rt-player-progress");
    player_audio.addEventListener("timeupdate", (e) => {
        player_progress.val(player_audio.currentTime/player_audio.duration);
    });
});
