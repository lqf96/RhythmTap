//[ Imports ]
//Libraries
import $ from "jquery";
import db from "db";

//[ Variables & Constants ]
//Database server
let db_server;
//Text decoder
let utf_8_decoder = new TextDecoder("utf-8");

//[ Functions ]
//Read file as text
export function read_as_text(f)
{   return new Promise((resolve) => {
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            resolve(reader.result);
        });
        reader.readAsText(f);
    });
}

//Read file as data URL
export function read_as_data_url(f)
{   return new Promise((resolve) => {
        let reader = new FileReader();
        reader.addEventListener("load", (event) => {
            resolve(reader.result);
        })
        reader.readAsDataURL(f);
    });
}

//Import beatmap
export async function import_beatmap(file_tree)
{   //Read meta information
    let beatmap = JSON.parse(await read_as_text(file_tree["RhythmTap.json"]));
    //Server
    let server = await db_promise;
    //Add to database
    beatmap = await server.beatmaps.add(beatmap).then((records) => records[0]);

    const d_prefix = `/Beatmaps/${beatmap.id}/Difficulties/`,
        d_dir = file_tree["Difficulties"];
    //Store difficulties
    await server.blobs.add(beatmap.difficulties.map((name) => ({
        path: d_prefix+name+".json",
        content: d_dir[name+".json"]
    })));

    const a_prefix = `/Beatmaps/${beatmap.id}/Assets/`;
    //Store assets
    await server.blobs.add($.map(
        file_tree["Assets"],
        (content, name) => ({
            path: a_prefix+name,
            content: content
        })
    ));

    return beatmap;
}

//Get all beatmaps
export function get_all_beatmaps()
{   return db_promise
        .then((server) => server.beatmaps.query().all().execute());
}

//Get file
export function get_file(path)
{   return db_promise
        .then((server) => server.blobs.get(path))
        .then((record) => record.content);
}

//[ Initialization ]
//Open database
let db_promise = db.open({
    server: "RhythmTap",
    version: 1,
    schema: {
        //File content
        blobs: {
            key: {
                keyPath: "path"
            },
            indexes: {
                content: {}
            }
        },
        //Beatmaps
        beatmaps: {
            key: {
                keyPath: "id",
                autoIncrement: true
            },
            indexes: {
                song: {},
                artist: {},
                version: {},
                difficulties: {},
                bgimage: {},
                track: {}
            }
        }
    }
}).then((server) => {
    db_server = server;
    return server;
});
