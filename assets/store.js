"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.import_beatmap = undefined;


//Import beatmap

var import_beatmap = exports.import_beatmap = function () {
    var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(file_tree) {
        var beatmap, server, d_prefix, d_dir, a_prefix;
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        _context.t0 = JSON;
                        _context.next = 3;
                        return read_as_text(file_tree["RhythmTap.json"]);

                    case 3:
                        _context.t1 = _context.sent;
                        beatmap = _context.t0.parse.call(_context.t0, _context.t1);
                        _context.next = 7;
                        return db_promise;

                    case 7:
                        server = _context.sent;
                        _context.next = 10;
                        return server.beatmaps.add(beatmap).then(function (records) {
                            return records[0];
                        });

                    case 10:
                        beatmap = _context.sent;
                        d_prefix = "/Beatmaps/" + beatmap.id + "/Difficulties/", d_dir = file_tree["Difficulties"];
                        //Store difficulties

                        _context.next = 14;
                        return server.blobs.add(beatmap.difficulties.map(function (name) {
                            return {
                                path: d_prefix + name + ".json",
                                content: d_dir[name + ".json"]
                            };
                        }));

                    case 14:
                        a_prefix = "/Beatmaps/" + beatmap.id + "/Assets/";
                        //Store assets

                        _context.next = 17;
                        return server.blobs.add(_jquery2.default.map(file_tree["Assets"], function (content, name) {
                            return {
                                path: a_prefix + name,
                                content: content
                            };
                        }));

                    case 17:
                        return _context.abrupt("return", beatmap);

                    case 18:
                    case "end":
                        return _context.stop();
                }
            }
        }, _callee, this);
    }));

    return function import_beatmap(_x) {
        return _ref.apply(this, arguments);
    };
}();

//Get all beatmaps


exports.read_as_text = read_as_text;
exports.read_as_data_url = read_as_data_url;
exports.get_all_beatmaps = get_all_beatmaps;
exports.get_file = get_file;

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

var _db = require("db");

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { return step("next", value); }, function (err) { return step("throw", err); }); } } return step("next"); }); }; } //[ Imports ]
//Libraries


//[ Variables & Constants ]
//Database server
var db_server = void 0;
//Text decoder
var utf_8_decoder = new TextDecoder("utf-8");

//[ Functions ]
//Read file as text
function read_as_text(f) {
    return new Promise(function (resolve) {
        var reader = new FileReader();
        reader.addEventListener("load", function (event) {
            resolve(reader.result);
        });
        reader.readAsText(f);
    });
}

//Read file as data URL
function read_as_data_url(f) {
    return new Promise(function (resolve) {
        var reader = new FileReader();
        reader.addEventListener("load", function (event) {
            resolve(reader.result);
        });
        reader.readAsDataURL(f);
    });
}function get_all_beatmaps() {
    return db_promise.then(function (server) {
        return server.beatmaps.query().all().execute();
    });
}

//Get file
function get_file(path) {
    return db_promise.then(function (server) {
        return server.blobs.get(path);
    }).then(function (record) {
        return record.content;
    });
}

//[ Initialization ]
//Open database
var db_promise = _db2.default.open({
    server: "RhythmTap",
    version: 1,
    schema: {
        //File content
        blobs: {
            key: {
                keyPath: "path"
            },
            indexes: {
                content: {}
            }
        },
        //Beatmaps
        beatmaps: {
            key: {
                keyPath: "id",
                autoIncrement: true
            },
            indexes: {
                song: {},
                artist: {},
                version: {},
                difficulties: {},
                bgimage: {},
                track: {}
            }
        }
    }
}).then(function (server) {
    db_server = server;
    return server;
});
//# sourceMappingURL=store.js.map