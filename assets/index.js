"use strict";

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

var _vue = require("vue");

var _vue2 = _interopRequireDefault(_vue);

var _store = require("./store");

var _gameplay = require("./gameplay");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//[ Constants ]
//Colors

//RhythmTap
//[ Imports ]
//Libraries
var colors = ["green", "blue", "purple", "red"];
//Current color
var current_color_index = 0;

//[ Functions ]
//Date time filter
_vue2.default.filter("rt-date-time", function (time) {
    //Seconds
    var sec = time.getSeconds();
    sec = sec < 10 ? "0" + sec : String(sec);
    //Minutes
    var min = time.getMinutes();
    min = min < 10 ? "0" + min : String(min);
    //Hours
    var hour = time.getHours();
    hour = hour < 10 ? "0" + hour : String(hour);

    return hour + ":" + min + ":" + sec;
});

//Time filter
_vue2.default.filter("rt-time", function (time) {
    //Seconds
    var sec = time % 60;
    sec = sec < 10 ? "0" + sec : String(sec);
    //Minutes
    var min = Math.floor(time / 60) % 60;
    min = min < 10 ? "0" + min : String(min);
    //Hours
    var hour = Math.floor(time / 3600);
    hour = hour < 10 ? "0" + hour : String(hour);

    return hour + ":" + min + ":" + sec;
});

//Run when scripts loaded
(0, _jquery2.default)(function () {
    //[ Constants & Variables ]
    //Scene class
    var scene_class = "rt-loading";
    //Begin time & current time
    var begin_time = Date.now();
    //Current beatmap
    var current_beatmap = null;
    //Player audio element
    var player_audio = new Audio();

    //[ Functions ]
    //File tree from entry
    function file_tree_from_entry(item) {
        //File
        if (item.isFile) return new Promise(function (resolve) {
            item.file(function (file) {
                resolve(file);
            });
        });
        //Folder
        else if (item.isDirectory) return new Promise(function (resolve) {
                item.createReader().readEntries(function (entries) {
                    entries = Array.from(entries);
                    Promise.all(entries.map(file_tree_from_entry)).then(function (files) {
                        var folder = {};
                        for (var i = 0; i < entries.length; i++) {
                            folder[entries[i].name] = files[i];
                        }resolve(folder);
                    });
                });
            });
    }

    //Vue element
    var app = new _vue2.default({
        el: "body",
        //Data
        data: {
            //Game status
            status: {
                //Current time
                current_time: new Date(),
                uptime: 0,
                total_beatmaps: 0,
                played_beatmaps: 0
            },
            //Beatmaps
            beatmaps: [],
            //Current beatmap
            current_beatmap: null,
            //Current beatmap index
            current_beatmap_index: null,
            //Current difficulty
            current_diff: null,
            //Player
            player: {
                playing: false,
                show_volume: false
            },
            //Game prompts
            gameplay: {
                prompt: "",
                combo: 0,
                score: 0,
                accuracy: 1,
                point_gained: 0,
                stat_300: 0,
                stat_100: 0,
                stat_50: 0,
                stat_0: 0
            }
        },
        //Methods
        methods: {
            //Scene class
            set_scene: function set_scene(_scene) {
                (0, _jquery2.default)(".rt-scene").removeClass(scene_class).addClass(_scene);
                scene_class = _scene;
            },
            //Show import beatmap
            __show_import_beatmap: function __show_import_beatmap() {
                (0, _jquery2.default)("#rt-import-beatmap").show();
            },
            //Hide beatmap
            __hide_import_beatmap: function __hide_import_beatmap() {
                (0, _jquery2.default)("#rt-import-beatmap").hide();
            },
            //Import beatmap (Drop event)
            __import_beatmap: function __import_beatmap(event) {
                var _this = this;

                //Traverse through selected files & folders
                var items = Array.from(event.dataTransfer.items);
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = items[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var item = _step.value;
                        item = item.webkitGetAsEntry();
                        if (item)
                            //Read file tree
                            file_tree_from_entry(item)
                            //Import beatmap
                            .then(_store.import_beatmap)
                            //Add beatmap to beatmap list
                            .then(function (beatmap) {
                                beatmap.__color = colors[current_color_index % colors.length];
                                current_color_index++;
                                //Add color to beatmap
                                _this.beatmaps.push(beatmap);
                                //Set current beatmap to this beatmap
                                _this.set_current_beatmap(_this.beatmaps.length - 1);
                            });
                    }
                    //Hide import beatmap prompt
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }

                (0, _jquery2.default)("#rt-import-beatmap").hide();
            },
            //Set current beatmap
            set_current_beatmap: function set_current_beatmap(index) {
                var _this2 = this;

                this.current_beatmap_index = index;
                var beatmap = this.current_beatmap = this.beatmaps[index];
                //Set difficulty
                this.set_difficulty(beatmap.difficulties[0]);

                //Set background image
                (0, _store.get_file)("/Beatmaps/" + beatmap.id + "/Assets/" + beatmap.bgimage).then(_store.read_as_data_url).then(function (url) {
                    (0, _jquery2.default)(document.body).css("background-image", "url(" + url + ")");
                });
                //Set player audio
                (0, _store.get_file)("/Beatmaps/" + beatmap.id + "/Assets/" + beatmap.track).then(_store.read_as_data_url).then(function (url) {
                    //Play music
                    player_audio.pause();
                    player_audio.src = url;
                    player_audio.play();
                    //Set playing state and duration
                    _this2.player.playing = true;
                    _this2.player.duration = player_audio.duration;
                });
            },
            //Play / pause audio
            play_pause_audio: function play_pause_audio() {
                var playing = this.player.playing = !this.player.playing;
                //Play
                if (playing) player_audio.play();
                //Pause
                else player_audio.pause();
            },
            //Show / hide volume
            show_hide_volume: function show_hide_volume() {
                this.player.show_volume = !this.player.show_volume;
            },
            //Set volume
            set_volume: function set_volume(event) {
                var volume_ratio = event.offsetX / event.target.offsetWidth;
                (0, _jquery2.default)("#rt-player-volume").val(volume_ratio);
                player_audio.volume = volume_ratio;
            },
            //Begin game play
            begin_gameplay: function begin_gameplay() {
                if (!this.current_diff) return;
                //Change scene and stop music
                this.set_scene("rt-game");
                if (this.player.playing) this.play_pause_audio();
                //Start gameplay
                new _gameplay.GamePlay(this);
            },
            //Set current difficulty
            set_difficulty: function set_difficulty(diff) {
                this.current_diff = diff;
            }
        }
    });

    //[ Game Initialization ]
    //Game scene
    app.set_scene("rt-welcome");

    //Game time
    setInterval(function () {
        //Current time
        var current_time = app.status.current_time = new Date();
        //Uptime
        app.status.uptime = Math.floor((current_time - begin_time) / 1000);
    }, 1000);

    //Retrive beatmap metadatas
    (0, _store.get_all_beatmaps)().then(function (beatmaps) {
        //Set beatmap colors
        var _iteratorNormalCompletion2 = true;
        var _didIteratorError2 = false;
        var _iteratorError2 = undefined;

        try {
            for (var _iterator2 = beatmaps[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                var beatmap = _step2.value;
                beatmap.__color = colors[current_color_index % colors.length];
                current_color_index++;
            }
            //Set beatmaps
        } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion2 && _iterator2.return) {
                    _iterator2.return();
                }
            } finally {
                if (_didIteratorError2) {
                    throw _iteratorError2;
                }
            }
        }

        app.beatmaps = beatmaps;

        if (beatmaps.length >= 1) app.set_current_beatmap(0);
    });

    //Set player audio
    player_audio.loop = true;
    //Song progress
    var player_progress = (0, _jquery2.default)("#rt-player-progress");
    player_audio.addEventListener("timeupdate", function (e) {
        player_progress.val(player_audio.currentTime / player_audio.duration);
    });
});
//# sourceMappingURL=index.js.map