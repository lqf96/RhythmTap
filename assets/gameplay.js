"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.GamePlay = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); //Libraries

//RhythmTap


exports.wait = wait;

var _two = require("two");

var _two2 = _interopRequireDefault(_two);

var _jquery = require("jquery");

var _jquery2 = _interopRequireDefault(_jquery);

var _vue = require("vue");

var _vue2 = _interopRequireDefault(_vue);

var _store = require("./store");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

//[ Variables & Constants ]
//Default beatmap parameters
var default_hp_drain = 0.01,
    default_judge = 1;
//Keys
var keys = ["d", "f", "j", "k"];
//Colors
var key_to_colors = {
    "d": "#00FF00",
    "f": "#0000FF",
    "j": "#FF00FF",
    "k": "#FF0000"
};
//Object status
var OBJ_LONG_TAP = 1,
    OBJ_MARKED = 2;
//Score status
var SCORE_DIGITS = 6;

//[ Helper Functions ]
//Wait for given time
function wait(time) {
    return new Promise(function (resolve) {
        setTimeout(resolve, time);
    });
}

//Game score filters
_vue2.default.filter("rt-score", function (score) {
    var result = String(score);
    while (result.length < SCORE_DIGITS) {
        result = "0" + result;
    }return result;
});

//Accuracy filters
_vue2.default.filter("rt-accuracy", function (accuracy) {
    return Math.round(accuracy * 10000) / 100 + "%";
});

//[ Classes ]
//Gameplay class

var GamePlay = exports.GamePlay = function () {
    //Create a new game

    function GamePlay(app) {
        var _this = this;

        _classCallCheck(this, GamePlay);

        //Set application
        this.app = app;
        //Initialize gameplay members
        app.combo = 0;
        app.score = 0;
        app.accuracy = 1;

        //Set up panel
        var two = this.two = new _two2.default({
            autostart: true,
            fullscreen: true
        });
        //Gameplay area
        var gameplay_area = (0, _jquery2.default)("#rt-gameplay"),
            width = gameplay_area.width(),
            height = gameplay_area.height();
        this.width = width;
        this.height = height;
        //Append to gameplay area
        two.appendTo(gameplay_area[0]);

        //Key to X coordinate
        var key_to_x = this.key_to_x = {
            "d": 0.275 * width,
            "f": 0.425 * width,
            "j": 0.575 * width,
            "k": 0.725 * width
        };
        //Default falling rate
        var default_falling_rate = 0.25 * height;
        //Track width
        var track_width = this.track_width = 0.15 * width;

        //Draw tracks
        var track_d = two.makeRectangle(key_to_x["d"], 0.5 * height, track_width, height),
            track_f = two.makeRectangle(key_to_x["f"], 0.5 * height, track_width, height),
            track_j = two.makeRectangle(key_to_x["j"], 0.5 * height, track_width, height),
            track_k = two.makeRectangle(key_to_x["k"], 0.5 * height, track_width, height);
        //Fill with colors, set opacity, no stroke
        track_d.fill = key_to_colors["d"];
        track_f.fill = key_to_colors["f"];
        track_j.fill = key_to_colors["j"];
        track_k.fill = key_to_colors["k"];
        track_d.opacity = track_f.opacity = track_j.opacity = track_k.opacity = 0.3;
        track_d.noStroke();
        track_f.noStroke();
        track_j.noStroke();
        track_k.noStroke();
        //Draw response area
        var resp_area = two.makeRectangle(0.5 * width, 0.85 * height, width, 0.05 * height);
        resp_area.opacity = 0.4;
        resp_area.noStroke();
        //Draw response area texts
        var text_options = {
            fill: "#FFF",
            family: "segoe-ui",
            size: 0.04 * height
        };
        two.add(new _two2.default.Text("D", key_to_x["d"], 0.855 * height, text_options));
        two.add(new _two2.default.Text("F", key_to_x["f"], 0.855 * height, text_options));
        two.add(new _two2.default.Text("J", key_to_x["j"], 0.855 * height, text_options));
        two.add(new _two2.default.Text("K", key_to_x["k"], 0.855 * height, text_options));
        //Update game area
        two.update();

        //Load beatmap
        (0, _store.get_file)("/Beatmaps/" + app.current_beatmap.id + "/Difficulties/" + app.current_diff + ".json").then(_store.read_as_text).then(function (content) {
            var diff = JSON.parse(content);
            //Store difficulty metadata
            _this.hp_drain = default_hp_drain * diff.hp_drain;
            var falling_rate = _this.falling_rate = default_falling_rate * diff.falling_rate;
            _this.judge = default_judge * diff.judge;

            //Prerender all object
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = diff.beatmap[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var o = _step.value;
                    var prerender = o.__prerender = {};
                    //X coordinate
                    prerender.x = key_to_x[o[2]];

                    //Short tap
                    if (o[0] == "st") {
                        //Height
                        prerender.height = 0.05 * height;
                        //Appear time
                        prerender.appear = o[1] - 875 * height / falling_rate;
                        //Disappear time
                        prerender.disappear = o[1] + 175 * height / falling_rate;
                    }
                    //Long tap
                    else {
                            prerender.height = (o[3] - o[1]) / 1000 * falling_rate;
                            prerender.appear = o[1] - 850 * height / falling_rate;
                            prerender.disappear = o[3] + 150 * height / falling_rate;
                        }

                    //Y coordinate
                    prerender.y = -0.5 * prerender.height;
                }
                //Set beatmap
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            _this.beatmap = diff.beatmap;

            //Current object index
            _this.obj_i = 0;
            //Active objects
            _this.active_objs = [];

            //Load music
            return (0, _store.get_file)("/Beatmaps/" + app.current_beatmap.id + "/Assets/" + app.current_beatmap.track);
        }).then(_store.read_as_data_url).then(function (url) {
            //Create audio object
            var audio = _this.audio = new Audio(url);
            //Wait until audio can be played
            return new Promise(function (resolve) {
                audio.addEventListener("canplay", function () {
                    resolve(audio);
                });
            });
        }).then(function (audio) {
            //Calculate animation begin time
            var animation_begin = Math.min(0, _this.beatmap[0].__prerender.appear);
            //Calculate reference time and previous time
            _this.zero_time = Date.now() - animation_begin;
            _this.prev_time = animation_begin;

            //Start animation
            two.update();
            two.bind("update", function () {
                _this.frame_update();
            }).play();
            //Start music
            wait(-1 * animation_begin).then(function () {
                audio.play();
            });

            //Listen to keyboard event
            _this.__key_down = function (e) {
                return _this.key_down_handler(e);
            };
            window.addEventListener("keydown", _this.__key_down);
            _this.__key_up = function (e) {
                return _this.key_up_handler(e);
            };
            window.addEventListener("keyup", _this.__key_up);
        });

        //Set game progress bar
        this.game_progress = (0, _jquery2.default)("#rt-game-progress-foreground");
        //HP & HP bar
        this.hp_fg = (0, _jquery2.default)("#rt-game-hp-foreground");
        this.hp = 1;

        //Point gained
        this.point_gained_panel = (0, _jquery2.default)("#rt-game-point-gained");
        //Key active rectangles
        this.key_active_rect = {};

        //Confirmed objects
        this.confirmed_objs = 0;
    }

    //Get current time


    _createClass(GamePlay, [{
        key: "get_time",
        value: function get_time() {
            //Audio not playing
            if (this.audio.currentTime <= 0 || this.audio.ended) return Date.now() - this.zero_time;
            //Playing
            else return this.audio.currentTime * 1000;
        }

        //Frame update

    }, {
        key: "frame_update",
        value: function frame_update() {
            //Get current audio time
            var time = this.get_time(),
                dt = time - this.prev_time;
            this.prev_time = time;
            //References
            var beatmap = this.beatmap,
                active_objs = this.active_objs,
                two = this.two;

            //Calculate new position
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
                for (var _iterator2 = active_objs[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                    var o = _step2.value;

                    o.translation.y += dt / 1000 * this.falling_rate;
                } //Remove elements
            } catch (err) {
                _didIteratorError2 = true;
                _iteratorError2 = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion2 && _iterator2.return) {
                        _iterator2.return();
                    }
                } finally {
                    if (_didIteratorError2) {
                        throw _iteratorError2;
                    }
                }
            }

            var i = active_objs.length - 1;
            while (i >= 0 && active_objs[i].__obj.__prerender.disappear < time) {
                //Not tapped
                if (!active_objs[i].__gameplay) {
                    this.update_score(0, "red");
                    this.add_hp(-0.04);
                }
                //Unhandled long tap
                else if (active_objs[i].__gameplay == OBJ_LONG_TAP) {
                        if (active_objs[i].__gameplay.good_begin) {
                            this.update_score(100, "green");
                            this.add_hp(0.02);
                        } else {
                            this.update_score(50, "yellow");
                            this.add_hp(0.01);
                        }
                    }

                //Removal
                active_objs[i].remove();
                active_objs.pop();
                i--;
            }

            //Insert new objects
            while (this.obj_i < beatmap.length && beatmap[this.obj_i].__prerender.appear < time) {
                //Current object and rectangle
                var current_obj = beatmap[this.obj_i],
                    rect = two.makeRectangle(current_obj.__prerender.x, current_obj.__prerender.y, this.track_width, current_obj.__prerender.height);

                //Set rectangle style
                rect.fill = current_obj[0] == "st" ? "#FFF" : key_to_colors[current_obj[2]];
                rect.opacity = 0.6;
                rect.noStroke();
                //Set corresponding object
                rect.__obj = current_obj;
                //Add to active objects
                active_objs.unshift(rect);

                this.obj_i++;
            }

            //End of game
            if (this.obj_i == beatmap.length && active_objs.length == 0) this.game_end();

            //Update song progress bar
            if (this.audio.currentTime > 0 && !this.audio.ended) this.game_progress.css("width", this.audio.currentTime / this.audio.duration * 100 + "%");
            //HP drain
            this.add_hp(-1 * this.hp_drain * dt / 1000);
        }

        //Add HP

    }, {
        key: "add_hp",
        value: function add_hp(d_hp) {
            this.hp = Math.min(this.hp + d_hp, 1);
            this.hp_fg.css("width", Math.max(0, this.hp) * 40 + "%");
            if (this.hp < 0) this.game_over();
        }

        //Handle key down event

    }, {
        key: "key_down_handler",
        value: function key_down_handler(event) {
            //Get input character
            var char = String.fromCharCode(event.keyCode).toLowerCase();
            if (keys.indexOf(char) == -1 || this.key_active_rect[char]) return;

            //Show key highlight rectangle
            var key_rect = this.key_active_rect[char] = this.two.makeRectangle(this.key_to_x[char], 0.85 * this.height, 0.15 * this.width, 0.05 * this.height);
            key_rect.noStroke();

            var next_obj = null;
            //Find last non-mark object
            for (var i = this.active_objs.length - 1; i >= 0; i--) {
                var o = this.active_objs[i];
                if (o.__obj[2] == char && o.__gameplay != OBJ_MARKED) {
                    next_obj = o;
                    break;
                }
            }
            //Not found
            if (!next_obj) return;

            //Short tap
            if (next_obj.__obj[0] == "st") {
                //Not in effective range
                var dy = Math.abs(next_obj.translation.y - 0.85 * this.height);
                if (dy > 0.05 * this.height) return;

                //50
                if (dy > 0.04176 * this.height) {
                    this.update_score(50, "yellow");
                    this.add_hp(0.01);
                }
                //100
                else if (dy > 0.03333 * this.height) {
                        this.update_score(100, "green");
                        this.add_hp(0.02);
                    }
                    //300
                    else {
                            this.update_score(300, "blue");
                            this.add_hp(0.04);
                        }

                //Set object to marked
                next_obj.__gameplay = OBJ_MARKED;
            }
            //Long tap
            else if (next_obj.__obj[0] == "lt") {
                    var rect_size = next_obj.getBoundingClientRect();
                    //Not in effective range
                    if (rect_size.top > 0.875 * this.height || rect_size.bottom < 0.825 * this.height) return;else {
                        var gameplay = next_obj.__gameplay = new Number(OBJ_LONG_TAP);
                        gameplay.good_begin = rect_size.bottom <= 0.875 * this.height;
                    }
                }
        }

        //Update score

    }, {
        key: "update_score",
        value: function update_score(point, color) {
            var _this2 = this;

            //Set point
            this.point_gained_panel.text(point).css("color", color).show();
            //Remove previous timer
            if (this.point_gained_timer) clearTimeout(this.point_gained_timer);

            //Hide panel
            this.point_gained_timer = setTimeout(function () {
                _this2.point_gained_panel.hide();
                _this2.point_gained_timer = null;
            }, 1000);

            //Update accuracy
            this.app.gameplay.accuracy = (this.app.gameplay.accuracy * this.confirmed_objs + point / 300) / (this.confirmed_objs + 1);
            //Update score (with bonus)
            this.app.gameplay.score += point * (this.app.gameplay.combo * 0.02 + 1);
            //Combo
            if (point > 0) this.app.gameplay.combo += 1;else this.app.gameplay.combo = 0;
            //Update statistics
            this.app.gameplay["stat_" + point] += 1;

            //Update confirmed objects
            this.confirmed_objs++;
        }

        //Handle key up event

    }, {
        key: "key_up_handler",
        value: function key_up_handler(event) {
            //Get input character
            var char = String.fromCharCode(event.keyCode).toLowerCase();
            if (keys.indexOf(char) == -1) return;

            //Remove key active rectangle
            this.key_active_rect[char].remove();
            this.key_active_rect[char] = null;

            //Find next long tap object
            var next_obj = null;
            for (var i = this.active_objs.length - 1; i >= 0; i--) {
                var o = this.active_objs[i];
                if (o.__obj[2] == char && o.__gameplay == OBJ_LONG_TAP) {
                    next_obj = o;
                    break;
                }
            }
            if (!next_obj) return;

            //Get bounding rectangle
            var obj_rect = next_obj.getBoundingClientRect(),
                good_end = obj_rect.top >= 0.825 * this.height && obj_rect.top <= 0.875 * this.height;
            //Set score
            if (good_end && next_obj.__gameplay.good_begin) {
                this.update_score(300, "blue");
                this.add_hp(0.04);
            } else if (good_end || next_obj.__gameplay.good_begin) {
                this.update_score(100, "green");
                this.add_hp(0.02);
            } else {
                this.update_score(50, "red");
                this.add_hp(0.01);
            }

            //Set object to marked
            next_obj.__gameplay = OBJ_MARKED;
        }

        //Game over

    }, {
        key: "game_over",
        value: function game_over() {
            var _this3 = this;

            //Stop animation and audio
            this.two.pause();
            this.audio.pause();

            //Set up failed game prompt and show it
            (0, _jquery2.default)("#rt-game-result-title").text("Failed");
            (0, _jquery2.default)("#rt-game-result-back").css("background-color", "orange");
            (0, _jquery2.default)("#rt-game-result-bg").css("width", "20rem").css("background-color", "yellow").show();
            (0, _jquery2.default)("#rt-game-result-fg").css("width", "20rem").show();

            //Do clean up when fully exit current game
            (0, _jquery2.default)("#rt-game-result-back").on("click", function () {
                _this3.clean_up();
                //Back to main menu
                _this3.app.set_scene("rt-main-panel");
                _this3.app.play_pause_audio();
            });
        }

        //Game end

    }, {
        key: "game_end",
        value: function game_end() {
            var _this4 = this;

            //Stop animation
            this.two.pause();

            //Calculate ranking
            var ranking = "F";
            if (this.app.gameplay.accuracy >= 0.9) ranking = this.app.gameplay.stat_0 == 0 ? "S" : "A";else if (this.app.gameplay.accuracy >= 0.8) ranking = "B";else if (this.app.gameplay.accuracy >= 0.7) ranking = "C";else if (this.app.gameplay.accuracy >= 0.6) ranking = "D";
            //Set up success game prompt and show it
            (0, _jquery2.default)("#rt-game-result-title").text("Success");
            (0, _jquery2.default)("#rt-game-result-ranking-cell").show();
            (0, _jquery2.default)("#rt-game-result-ranking").text(ranking);
            (0, _jquery2.default)("#rt-game-result-back").css("background-color", "deepskyblue");
            (0, _jquery2.default)("#rt-game-result-bg").css("width", "25rem").css("background-color", "aqua").show();
            (0, _jquery2.default)("#rt-game-result-fg").css("width", "25rem").show();

            //Do clean up when fully exit current game
            (0, _jquery2.default)("#rt-game-result-back").on("click", function () {
                _this4.clean_up();
                //Back to main menu
                _this4.app.set_scene("rt-main-panel");
                _this4.app.play_pause_audio();
            });
        }

        //Game clean up

    }, {
        key: "clean_up",
        value: function clean_up() {
            //Clean up all event handlers
            window.removeEventListener("keydown", this.__key_down);
            window.removeEventListener("keyup", this.__key_up);
            (0, _jquery2.default)("#rt-game-result-back").off("click");
            //Reset application data
            this.app.gameplay.prompt = "";
            this.app.gameplay.combo = 0;
            this.app.gameplay.score = 0;
            this.app.gameplay.accuracy = 1;
            this.app.gameplay.point_gained = 0;
            this.app.gameplay.stat_300 = 0;
            this.app.gameplay.stat_100 = 0;
            this.app.gameplay.stat_50 = 0;
            this.app.gameplay.stat_0 = 0;
            //Reset game status panel
            (0, _jquery2.default)("#rt-game-result-ranking-cell").hide();
            (0, _jquery2.default)("#rt-game-result-bg").hide();
            (0, _jquery2.default)("#rt-game-result-fg").hide();
            //Reset progress bars
            this.game_progress.css("width", 0);
            this.hp_fg.css("width", "40%");
            //Clear two instance
            this.two.clear();
        }
    }]);

    return GamePlay;
}();
//# sourceMappingURL=gameplay.js.map