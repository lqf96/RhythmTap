# RhythmTap (WFT Course Assignment) Report
Qifan Lu (2014013423) & Zhangjie Cao (2014013428)

## What is RhythmTap?
* RhythmTap is a simple Rhythm game which has four note tracks in gameplay and allows you to DIY your own beatmaps.
* Highlighted features include:
  - Make your own beatmaps and import your beatmaps to the game by dragging beatmap folder into the browser.
  - One beatmap can contain multiple difficulties, making a single song more interesting.
  - Gain different scores for different performance on a single note.
  - More challenging gameplay with HP value and constant HP value drain.
  - Game statistics shown when gameplay finished or when player has failed.
  - Modern-like UI design makes the game UI neat and cool.
* Rules
  - Each beatmap has four tracks, and players respond to notes on different tracks by pressing stroke "d", "f", "j" and "k".
  - Notes are divided into "short tap" and "long tap" based on how long the key should be pressed down.
  - For short tap notes players gain 300 points and 4% HP if their hitting error is within 2/3 of the threshold, 100 points and 2% HP between 2/3 and 5/6 of the threshold or 50 points and 1% HP between 6/5 and one time of the threshold. Otherwise, they hit nothing and each short tap note ignored reduce HP value by 4%.
  - For long tap notes players gain a "good beginning" if the time they press down the button minus the time the long tap note begins is within a threshold, or a "bad beginning" if they just press down the key during the long tap note duration. Similarly the player gains a "good ending" if the time they release the key minus long tap note ending time is within a threshold, or else they gains a "bad ending". Players who get both a "good beginning" and a "good ending" get 300 points and a 4% HP increase, or 100 points and a 2% HP  increase with either a "good beginning" or a "good ending", or 50 points and a 1% HP boost with neither "good beginning" nor "good ending".
  - The HP value drains at the rate of 2% each second.
  - Each single note score is multiplied with a combo factor, whose formula is `factor = 1 + combo * 0.02`.
  - Total accuracy is defined as the average accuracy of each single note. For each single note, 300 points corresponds to 100% accuracy, 100 points corresponds to 33.33% accuracy, 50 points corresponds to 16.67% accuracy, and 0 points corresponds to 0 accuracy.

## Project Information
* Language: Javascript (ES6 & ESNext)
* Third party libraries:
  - jQuery: DOM manipulation, Event handling & Helper functions
  - Vue.js: Two-way data binding
  - Two.js: 2D drawing library with canvas, SVG and WebGL backend
  - Db.js: IndexedDB wrapper library
* Development environment
  - Node & NPM: Build system
  - Babel: Javascript transpiler
  - Http-Server: Serving static files from localhost
* Repository: https://coding.net/u/lqf96/p/RhythmTap/git
* Demo URL: https://http451-store.surge.sh/RhythmTap

## Tachnical Trials
* HTML5 File Drag & Drop API
  - "webkitGetAsEntry()" extension for walking through folders dropped in.
  - File APIs and data URLs serve as the foundation of this project.
* IndexedDB (using "db.js")
  - IndexedDB as local backend for storing beatmap metadata, data and assets.
* HTML5 Audio API
  - Preload audio to avoid latency
* Optimization: Shape position calculation  in advance.
* Promise, ESNext `async` / `await` syntax and asynchronous programming.
* ...

## Problems, Solutions & Thoughts
* Making audio track and beatmap in sync
* CSS class, game scenes and scene transitions
* Problems to be improved

## Roles
* Qifan Lu
  - Loading, Welcome & Main UI
  - Main UI Logics
  - File Drag & Drop
  - Gameplay UI
* Zhangjie Cao
  - Beatmap Data Storage Backend
  - Gameplay Logics
  - Game Over & Game End Interface
